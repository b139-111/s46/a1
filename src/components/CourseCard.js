import {useState} from 'react';
import PropTypes from 'prop-types'
import {Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}){
    console.log(courseProp)
  const {name, description, price} = courseProp;

  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);
  console.log(useState(0))

///////////////////////////

  
  function enroll(){
    if (seats !== 0){
      setSeats(seats - 1);
      setCount(count + 1);
    }
    else{
      alert('No more seats available');
    }
  }
/////////////////////////////
  return (

  <Card className="highlightCard p-3 mt-4">
    <Card.Body>
      <Card.Title>{name}</Card.Title>
      <Card.Subtitle className="mb-2">
       Description:
      </Card.Subtitle>
   
        <Card.Text>{description}</Card.Text>
        <Card.Text className="mb-0">Price:</Card.Text>
        <Card.Text>{price}</Card.Text>
        <Card.Text>Enrollees: {count}</Card.Text>

      <Button variant="primary" onClick={enroll}>Enroll</Button>
    </Card.Body>
  </Card>


    )

}
CourseCard.propTypes = {
  //shape method check if a prop object confirms to a specific shape
  courseProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}
