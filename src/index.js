import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

/*ReactDOM.render(
  <h1>Hi, im jy</h1>, document.getElementById('root')
);*/

/*const name = "ken"
const element = <h1>Hello, ${name}</h1>
ReactDOM.render(
  element,
  document.getElementById('root')
);*/

/*let newObject;
const object = {firstname: "keanu", lastname: "orig"}
function first(){
  
  let newObject = `${object.firstname} ${object.lastname}`;

  return newObject
}
const element = <h1>Hello, { first()}</h1>

ReactDOM.render(
    element, document.getElementById('root')
  );*/


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

